EESchema Schematic File Version 4
LIBS:Inverter-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "unitrac"
Date ""
Rev "r0"
Comp "(C) 2019, Christian Vorobev Private Research"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:NUCLEO64-F411RE U9
U 1 1 5D53391B
P 3300 3700
F 0 "U9" H 3300 5881 50  0000 C CNN
F 1 "NUCLEO64" H 3300 5790 50  0000 C CNN
F 2 "ST-Morpho64:ST_Morpho_Connector_144_STLink" H 3850 1800 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/data_brief/DM00105918.pdf" H 2400 2300 50  0001 C CNN
	1    3300 3700
	1    0    0    -1  
$EndComp
Text HLabel 4400 3000 2    50   Input ~ 0
MCU_AHO
Text HLabel 4400 3100 2    50   Input ~ 0
MCU_ALO
Text HLabel 4400 3200 2    50   Input ~ 0
MCU_BHO
Text HLabel 4400 3300 2    50   Input ~ 0
MCU_BLO
Text HLabel 2200 4600 0    50   Input ~ 0
MCU_CHO
Text HLabel 2200 4700 0    50   Input ~ 0
MCU_CLO
Text HLabel 4400 2200 2    50   Input ~ 0
MCU_TRAC_FAULT_IN
Text HLabel 4400 2300 2    50   Input ~ 0
MCU_TRAC_EN_OUT
Text HLabel 2200 4400 0    50   Input ~ 0
MCU_TRAC_CHOP_OUT
Text HLabel 4400 5100 2    50   Input ~ 0
MCU_ATOT_IN
Text HLabel 2200 3800 0    50   Input ~ 0
MCU_APA_IN
Text HLabel 2200 3900 0    50   Input ~ 0
MCU_APB_IN
Text HLabel 4400 5000 2    50   Input ~ 0
MCU_ACHOP_IN
Text HLabel 3100 5950 3    50   Input ~ 0
MCU_VSS
Text HLabel 3800 5700 3    50   Input ~ 0
MCU_AVSS
Text HLabel 3800 1700 1    50   Input ~ 0
MCU_AVDD
Wire Wire Line
	2700 5700 2800 5700
Connection ~ 2800 5700
Wire Wire Line
	2800 5700 2900 5700
Connection ~ 2900 5700
Wire Wire Line
	2900 5700 3000 5700
Connection ~ 3000 5700
Wire Wire Line
	3000 5700 3100 5700
Connection ~ 3100 5700
Wire Wire Line
	3100 5700 3200 5700
Connection ~ 3200 5700
Wire Wire Line
	3200 5700 3300 5700
Connection ~ 3300 5700
Wire Wire Line
	3300 5700 3400 5700
Connection ~ 3400 5700
Wire Wire Line
	3400 5700 3500 5700
Connection ~ 3500 5700
Wire Wire Line
	3500 5700 3600 5700
Wire Wire Line
	3100 5700 3100 5950
$Comp
L Switch:SW_Push SW1
U 1 1 5DA01A75
P 1800 2300
F 0 "SW1" H 1800 2585 50  0000 C CNN
F 1 "SW_Push" H 1800 2494 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 1800 2500 50  0001 C CNN
F 3 "~" H 1800 2500 50  0001 C CNN
	1    1800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2300 2200 2300
Text Label 3100 5850 0    50   ~ 0
DVSS
Text Label 1600 2300 2    50   ~ 0
DVSS
NoConn ~ 4400 2400
NoConn ~ 4400 2500
NoConn ~ 4400 2600
NoConn ~ 4400 2700
NoConn ~ 4400 2800
NoConn ~ 4400 2900
NoConn ~ 4400 3400
NoConn ~ 4400 3500
NoConn ~ 4400 3600
NoConn ~ 4400 3700
NoConn ~ 4400 3900
NoConn ~ 4400 4000
NoConn ~ 4400 4100
NoConn ~ 4400 4200
NoConn ~ 4400 4300
NoConn ~ 4400 4400
NoConn ~ 4400 4500
NoConn ~ 4400 4600
NoConn ~ 4400 4700
NoConn ~ 4400 4800
NoConn ~ 4400 4900
NoConn ~ 4400 5200
NoConn ~ 2200 5300
NoConn ~ 2200 5200
NoConn ~ 2200 5100
NoConn ~ 2200 5000
NoConn ~ 2200 4900
NoConn ~ 2200 4800
NoConn ~ 2200 4500
NoConn ~ 2200 4300
NoConn ~ 2200 4200
NoConn ~ 2200 4100
NoConn ~ 2200 4000
NoConn ~ 2200 3600
NoConn ~ 2200 3400
NoConn ~ 2200 3300
NoConn ~ 2200 2200
NoConn ~ 2700 1700
NoConn ~ 2800 1700
NoConn ~ 2900 1700
NoConn ~ 3000 1700
NoConn ~ 3100 1700
NoConn ~ 3200 1700
NoConn ~ 3300 1700
NoConn ~ 3400 1700
NoConn ~ 4400 5300
$EndSCHEMATC
