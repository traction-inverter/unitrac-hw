EESchema Schematic File Version 4
LIBS:Inverter-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "unitrac"
Date ""
Rev "r0"
Comp "(C) 2019, Christian Vorobev Private Research"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2700 4450 0    50   ~ 0
Microcontroller Voltage Supply
Text Notes 2750 1750 0    50   ~ 0
Main Logic Voltage Supply
Text Notes 2700 3000 0    50   ~ 0
Analog Conditioning
$Comp
L Device:R_Small R11
U 1 1 5DA55C89
P 3750 3550
F 0 "R11" H 3809 3596 50  0000 L CNN
F 1 "1k" H 3809 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3750 3550 50  0001 C CNN
F 3 "~" H 3750 3550 50  0001 C CNN
	1    3750 3550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC547 Q8
U 1 1 5DA56213
P 4100 3500
F 0 "Q8" V 4428 3500 50  0000 C CNN
F 1 "BC547" V 4337 3500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4300 3425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 4100 3500 50  0001 L CNN
	1    4100 3500
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:BC547 Q9
U 1 1 5DA59485
P 4950 3500
F 0 "Q9" V 5278 3500 50  0000 C CNN
F 1 "BC547" V 5187 3500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5150 3425 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 4950 3500 50  0001 L CNN
	1    4950 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5DA5AC44
P 4600 3550
F 0 "R13" H 4659 3596 50  0000 L CNN
F 1 "1k" H 4659 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4600 3550 50  0001 C CNN
F 3 "~" H 4600 3550 50  0001 C CNN
	1    4600 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C26
U 1 1 5DA5D34F
P 4950 3900
F 0 "C26" H 5042 3946 50  0000 L CNN
F 1 "100uF" H 5042 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4950 3900 50  0001 C CNN
F 3 "~" H 4950 3900 50  0001 C CNN
	1    4950 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C24
U 1 1 5DA5DF4B
P 4100 3900
F 0 "C24" H 4192 3946 50  0000 L CNN
F 1 "100uF" H 4192 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4100 3900 50  0001 C CNN
F 3 "~" H 4100 3900 50  0001 C CNN
	1    4100 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3400 3900 3400
Wire Wire Line
	4300 3400 4600 3400
Wire Wire Line
	4600 3450 4600 3400
Connection ~ 4600 3400
Wire Wire Line
	4600 3400 4750 3400
Wire Wire Line
	4600 3650 4600 3700
Wire Wire Line
	4600 3700 4950 3700
Wire Wire Line
	4950 3700 4950 3800
Connection ~ 4950 3700
Wire Wire Line
	4100 3700 4100 3800
Wire Wire Line
	4100 3700 3750 3700
Connection ~ 4100 3700
Wire Wire Line
	4950 4000 4950 4100
Wire Wire Line
	4950 4100 4100 4100
Wire Wire Line
	4100 4100 4100 4000
Wire Wire Line
	3750 3700 3750 3650
Wire Wire Line
	3750 3450 3750 3400
Connection ~ 4950 4100
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5DA633C4
P 3750 4100
F 0 "FB1" V 3513 4100 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 3604 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 3680 4100 50  0001 C CNN
F 3 "~" H 3750 4100 50  0001 C CNN
	1    3750 4100
	0    1    1    0   
$EndComp
Wire Notes Line
	6100 2900 6100 4250
$Comp
L Device:C_Small C18
U 1 1 5DA670E6
P 3400 2100
F 0 "C18" H 3492 2146 50  0000 L CNN
F 1 "2.2uF" H 3492 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3400 2100 50  0001 C CNN
F 3 "~" H 3400 2100 50  0001 C CNN
	1    3400 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C21
U 1 1 5DA67E04
P 3600 2300
F 0 "C21" H 3692 2346 50  0000 L CNN
F 1 "2.2uF" H 3692 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3600 2300 50  0001 C CNN
F 3 "~" H 3600 2300 50  0001 C CNN
	1    3600 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C22
U 1 1 5DA6835D
P 3800 2500
F 0 "C22" H 3892 2546 50  0000 L CNN
F 1 "2.2uF" H 3892 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3800 2500 50  0001 C CNN
F 3 "~" H 3800 2500 50  0001 C CNN
	1    3800 2500
	1    0    0    -1  
$EndComp
$Comp
L Inverter-rescue:LM5163-Q1-Regulator_Switching U8
U 1 1 5DA72D4C
P 4750 2250
AR Path="/5DA72D4C" Ref="U8"  Part="1" 
AR Path="/5D532DF7/5DA72D4C" Ref="U8"  Part="1" 
F 0 "U8" H 4750 2665 50  0000 C CNN
F 1 "LM5163-Q1" H 4750 2574 50  0000 C CNN
F 2 "Package_SO:TI_SO-PowerPAD-8_ThermalVias" H 4750 2550 50  0001 C CNN
F 3 "" H 4750 2550 50  0001 C CNN
	1    4750 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1900 3400 1900
Wire Wire Line
	3200 2700 3400 2700
Wire Wire Line
	3800 2600 3800 2700
Wire Wire Line
	3600 2400 3600 2700
Connection ~ 3600 2700
Wire Wire Line
	3600 2700 3800 2700
Wire Wire Line
	3400 2200 3400 2700
Connection ~ 3400 2700
Wire Wire Line
	3400 2700 3600 2700
Wire Wire Line
	3400 1900 3400 2000
Connection ~ 3400 1900
Wire Wire Line
	3400 1900 3600 1900
Wire Wire Line
	3600 2200 3600 1900
Connection ~ 3600 1900
Wire Wire Line
	3600 1900 3800 1900
Wire Wire Line
	3800 2400 3800 1900
Connection ~ 3800 1900
Wire Wire Line
	3800 2700 4000 2700
Connection ~ 3800 2700
$Comp
L Device:R_Small R12
U 1 1 5DA7B10E
P 4150 2550
F 0 "R12" H 4209 2596 50  0000 L CNN
F 1 "46.4k" H 4209 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4150 2550 50  0001 C CNN
F 3 "~" H 4150 2550 50  0001 C CNN
	1    4150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2650 4150 2700
Wire Wire Line
	4150 2700 4000 2700
Connection ~ 4000 2700
Wire Wire Line
	4150 1900 4150 2200
Wire Wire Line
	4150 2200 4350 2200
Wire Wire Line
	3800 1900 4150 1900
Wire Wire Line
	4000 2100 4350 2100
Wire Wire Line
	4000 2100 4000 2700
Wire Wire Line
	4350 2300 4150 2300
Wire Wire Line
	4150 2300 4150 2200
Connection ~ 4150 2200
Wire Wire Line
	4350 2400 4150 2400
Wire Wire Line
	4150 2400 4150 2450
Wire Wire Line
	5150 2200 5250 2200
Wire Wire Line
	5450 2200 5550 2200
Wire Wire Line
	5550 2200 5550 2100
Wire Wire Line
	5550 2100 5150 2100
$Comp
L pspice:INDUCTOR L1
U 1 1 5DA82893
P 5950 2100
F 0 "L1" H 5950 2315 50  0000 C CNN
F 1 "SRN6045TA-330M" H 5950 2224 50  0000 C CNN
F 2 "Inductor_SMD:L_Bourns_SRN6045TA" H 5950 2100 50  0001 C CNN
F 3 "~" H 5950 2100 50  0001 C CNN
	1    5950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2100 5700 2100
Connection ~ 5550 2100
$Comp
L Device:R_Small R14
U 1 1 5DA84356
P 5750 2200
F 0 "R14" V 5550 2200 50  0000 C CNN
F 1 "309k" V 5650 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5750 2200 50  0001 C CNN
F 3 "~" H 5750 2200 50  0001 C CNN
	1    5750 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C28
U 1 1 5DA85415
P 6000 2350
F 0 "C28" H 6100 2450 50  0000 R CNN
F 1 "47pF" H 5950 2450 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 6000 2350 50  0001 C CNN
F 3 "~" H 6000 2350 50  0001 C CNN
	1    6000 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 2200 6000 2200
Wire Wire Line
	6000 2200 6000 2250
Wire Wire Line
	6000 2450 6000 2500
Wire Wire Line
	6000 2500 5350 2500
Wire Wire Line
	5350 2500 5350 2400
Wire Wire Line
	5350 2400 5150 2400
Wire Wire Line
	5550 2200 5650 2200
Connection ~ 5550 2200
$Comp
L Device:C_Small C29
U 1 1 5DA89D3E
P 6200 2200
F 0 "C29" V 6250 2400 50  0000 C CNN
F 1 "1.2n" V 6350 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6200 2200 50  0001 C CNN
F 3 "~" H 6200 2200 50  0001 C CNN
	1    6200 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 2200 6100 2200
Connection ~ 6000 2200
Wire Wire Line
	6200 2100 6450 2100
Wire Wire Line
	6450 2100 6450 2200
Wire Wire Line
	6450 2200 6300 2200
$Comp
L Device:R_Small R15
U 1 1 5DA8D6B3
P 6700 2250
F 0 "R15" H 6759 2296 50  0000 L CNN
F 1 "453k" H 6759 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6700 2250 50  0001 C CNN
F 3 "~" H 6700 2250 50  0001 C CNN
	1    6700 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R16
U 1 1 5DA8E578
P 6700 2550
F 0 "R16" H 6759 2596 50  0000 L CNN
F 1 "49.9k" H 6759 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6700 2550 50  0001 C CNN
F 3 "~" H 6700 2550 50  0001 C CNN
	1    6700 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2700 6700 2700
Wire Wire Line
	6700 2700 6700 2650
Connection ~ 4150 2700
Wire Wire Line
	6700 2450 6700 2400
Wire Wire Line
	6700 2150 6700 2100
Wire Wire Line
	6700 2100 6450 2100
Connection ~ 6450 2100
Wire Wire Line
	6000 2500 6450 2500
Wire Wire Line
	6450 2500 6450 2400
Wire Wire Line
	6450 2400 6700 2400
Connection ~ 6000 2500
Connection ~ 6700 2400
Wire Wire Line
	6700 2400 6700 2350
Connection ~ 6700 2700
$Comp
L Device:C_Small C31
U 1 1 5DA9915E
P 7300 2300
F 0 "C31" H 7392 2346 50  0000 L CNN
F 1 "4.7u" H 7392 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7300 2300 50  0001 C CNN
F 3 "~" H 7300 2300 50  0001 C CNN
	1    7300 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C30
U 1 1 5DA996C8
P 7100 2100
F 0 "C30" H 7192 2146 50  0000 L CNN
F 1 "4.7u" H 7192 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7100 2100 50  0001 C CNN
F 3 "~" H 7100 2100 50  0001 C CNN
	1    7100 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C32
U 1 1 5DA9ADEB
P 7500 2500
F 0 "C32" H 7592 2546 50  0000 L CNN
F 1 "4.7u" H 7592 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7500 2500 50  0001 C CNN
F 3 "~" H 7500 2500 50  0001 C CNN
	1    7500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2100 6700 1900
Wire Wire Line
	6700 1900 7100 1900
Connection ~ 6700 2100
Wire Wire Line
	7100 2000 7100 1900
Connection ~ 7100 1900
Wire Wire Line
	7100 1900 7300 1900
Wire Wire Line
	7100 2200 7100 2700
Wire Wire Line
	6700 2700 7100 2700
Wire Wire Line
	7100 2700 7300 2700
Connection ~ 7100 2700
Wire Wire Line
	7500 2600 7500 2700
Connection ~ 7500 2700
Wire Wire Line
	7300 2400 7300 2700
Connection ~ 7300 2700
Wire Wire Line
	7300 2700 7500 2700
Wire Wire Line
	7300 2200 7300 1900
Connection ~ 7300 1900
Wire Wire Line
	7300 1900 7500 1900
Wire Wire Line
	7500 2400 7500 1900
Connection ~ 7500 1900
NoConn ~ 5150 2300
Text Label 7950 2150 0    50   ~ 0
ANALOG_SECTION_POS
Text Label 7950 2450 0    50   ~ 0
ANALOG_SECTION_NEG
Wire Notes Line
	2650 1650 2650 2800
Wire Notes Line
	2650 2800 9200 2800
Wire Notes Line
	9200 2800 9200 1650
Wire Notes Line
	9200 1650 2650 1650
Text HLabel 1300 2550 3    50   Input ~ 0
PWR_MOTOR_NEG
Wire Notes Line
	2650 4250 2650 2900
$Comp
L Regulator_Linear:AMS1117-3.3 U7
U 1 1 5DADCB48
P 4200 4700
F 0 "U7" H 4200 4942 50  0000 C CNN
F 1 "AMS1117-3.3" H 4200 4851 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 4200 4900 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 4300 4450 50  0001 C CNN
	1    4200 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB2
U 1 1 5DADF137
P 5050 4950
F 0 "FB2" V 5000 5100 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 4900 5000 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 4980 4950 50  0001 C CNN
F 3 "~" H 5050 4950 50  0001 C CNN
	1    5050 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 5000 4200 5100
Wire Wire Line
	4500 4700 4550 4700
Text HLabel 5800 5100 2    50   Input ~ 0
PWR_MCU_VSS
Connection ~ 4200 5100
$Comp
L Device:C_Small C25
U 1 1 5DAF4328
P 4550 4900
F 0 "C25" H 4642 4946 50  0000 L CNN
F 1 "22u" H 4642 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4550 4900 50  0001 C CNN
F 3 "~" H 4550 4900 50  0001 C CNN
	1    4550 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4700 4550 4800
Connection ~ 4550 4700
Wire Wire Line
	4550 5000 4550 5100
Connection ~ 4550 5100
Wire Wire Line
	4550 5100 4200 5100
Wire Notes Line
	6100 4350 6100 5200
Wire Notes Line
	2650 5200 2650 4350
Text HLabel 1300 2000 1    50   Input ~ 0
PWR_MOTOR_POS
Wire Wire Line
	4550 5100 4800 5100
Wire Wire Line
	4950 4950 4800 4950
Wire Wire Line
	4800 4950 4800 5100
Connection ~ 4800 5100
Wire Wire Line
	4550 4700 4550 4550
$Comp
L Device:C_Small C19
U 1 1 5D5A1E4C
P 3550 4900
F 0 "C19" H 3350 4950 50  0000 L CNN
F 1 "10u" H 3300 4850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3550 4900 50  0001 C CNN
F 3 "~" H 3550 4900 50  0001 C CNN
	1    3550 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C20
U 1 1 5D5A2A4C
P 3700 4900
F 0 "C20" H 3792 4946 50  0000 L CNN
F 1 "100n" H 3800 4850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3700 4900 50  0001 C CNN
F 3 "~" H 3700 4900 50  0001 C CNN
	1    3700 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4800 3700 4700
Connection ~ 3700 4700
Wire Wire Line
	3700 4700 3900 4700
Wire Wire Line
	3550 4800 3550 4700
Wire Wire Line
	3550 4700 3700 4700
Wire Wire Line
	3700 5000 3700 5100
Connection ~ 3700 5100
Wire Wire Line
	3700 5100 4200 5100
Wire Wire Line
	3550 5000 3550 5100
Wire Wire Line
	3550 5100 3700 5100
Wire Wire Line
	3850 4100 4100 4100
Connection ~ 4100 4100
$Comp
L Device:C_Small C23
U 1 1 5D9D5809
P 3900 2200
F 0 "C23" H 3992 2246 50  0000 L CNN
F 1 "100n" H 3992 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3900 2200 50  0001 C CNN
F 3 "~" H 3900 2200 50  0001 C CNN
	1    3900 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2100 4000 2100
Connection ~ 4000 2100
Wire Wire Line
	3900 2300 4150 2300
Connection ~ 4150 2300
$Comp
L power:PWR_FLAG #FLG0111
U 1 1 5D5BEE93
P 5250 2200
F 0 "#FLG0111" H 5250 2275 50  0001 C CNN
F 1 "PWR_FLAG" H 5250 2373 50  0000 C CNN
F 2 "" H 5250 2200 50  0001 C CNN
F 3 "~" H 5250 2200 50  0001 C CNN
	1    5250 2200
	-1   0    0    1   
$EndComp
Connection ~ 5250 2200
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5D5B4680
P 1000 2200
F 0 "J6" H 1080 2242 50  0000 L CNN
F 1 "Conn_01x01" H 1080 2151 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 1000 2200 50  0001 C CNN
F 3 "~" H 1000 2200 50  0001 C CNN
	1    1000 2200
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 5D5B533E
P 1000 2350
F 0 "J7" H 1080 2392 50  0000 L CNN
F 1 "Conn_01x01" H 1080 2301 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 1000 2350 50  0001 C CNN
F 3 "~" H 1000 2350 50  0001 C CNN
	1    1000 2350
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C27
U 1 1 5DA80470
P 5350 2200
F 0 "C27" V 5121 2200 50  0000 C CNN
F 1 "2.2n X7R" V 5212 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5350 2200 50  0001 C CNN
F 3 "~" H 5350 2200 50  0001 C CNN
	1    5350 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 2200 1300 2000
Connection ~ 1300 2200
Wire Wire Line
	1300 2200 1200 2200
Wire Wire Line
	1300 2550 1300 2350
Connection ~ 1300 2350
Wire Wire Line
	1300 2350 1200 2350
Text HLabel 5800 3400 2    50   Input ~ 0
PWR_TRAC_AVDD
Text HLabel 5800 4100 2    50   Input ~ 0
PWR_TRAC_AVSS
Text HLabel 5800 4550 2    50   Input ~ 0
PWR_MCU_VDD
Text HLabel 5800 4950 2    50   Input ~ 0
PWR_MCU_AVSS
Text HLabel 8500 1900 2    50   Input ~ 0
PWR_GDRV_VDD
Text HLabel 8500 2700 2    50   Input ~ 0
PWR_GDRV_VSS
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DE5C3FD
P 5750 5100
F 0 "#FLG0101" H 5750 5175 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 5273 50  0001 C CNN
F 2 "" H 5750 5100 50  0001 C CNN
F 3 "~" H 5750 5100 50  0001 C CNN
	1    5750 5100
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DE5CC40
P 5750 4950
F 0 "#FLG0102" H 5750 5025 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 5123 50  0001 C CNN
F 2 "" H 5750 4950 50  0001 C CNN
F 3 "~" H 5750 4950 50  0001 C CNN
	1    5750 4950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5DE5D715
P 5750 4100
F 0 "#FLG0104" H 5750 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 4273 50  0001 C CNN
F 2 "" H 5750 4100 50  0001 C CNN
F 3 "~" H 5750 4100 50  0001 C CNN
	1    5750 4100
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5DE5DAD6
P 5750 3400
F 0 "#FLG0105" H 5750 3475 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 3573 50  0001 C CNN
F 2 "" H 5750 3400 50  0001 C CNN
F 3 "~" H 5750 3400 50  0001 C CNN
	1    5750 3400
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0106
U 1 1 5DE5E11B
P 8400 2700
F 0 "#FLG0106" H 8400 2775 50  0001 C CNN
F 1 "PWR_FLAG" V 8400 2828 50  0001 L CNN
F 2 "" H 8400 2700 50  0001 C CNN
F 3 "~" H 8400 2700 50  0001 C CNN
	1    8400 2700
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0107
U 1 1 5DE5E549
P 8400 1900
F 0 "#FLG0107" H 8400 1975 50  0001 C CNN
F 1 "PWR_FLAG" V 8400 2200 50  0001 C CNN
F 2 "" H 8400 1900 50  0001 C CNN
F 3 "~" H 8400 1900 50  0001 C CNN
	1    8400 1900
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0108
U 1 1 5DE5E931
P 1300 2000
F 0 "#FLG0108" H 1300 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 1300 2173 50  0001 C CNN
F 2 "" H 1300 2000 50  0001 C CNN
F 3 "~" H 1300 2000 50  0001 C CNN
	1    1300 2000
	0    1    -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0109
U 1 1 5DE5ECB9
P 1300 2550
F 0 "#FLG0109" H 1300 2625 50  0001 C CNN
F 1 "PWR_FLAG" H 1300 2723 50  0001 C CNN
F 2 "" H 1300 2550 50  0001 C CNN
F 3 "~" H 1300 2550 50  0001 C CNN
	1    1300 2550
	0    1    -1   0   
$EndComp
$Comp
L Device:Net-Tie_2 NT1
U 1 1 5DECDC37
P 1900 2200
F 0 "NT1" V 1950 2350 50  0001 C CNN
F 1 "NT_BAT_LOGIC_POS" V 1850 2600 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 1900 2200 50  0001 C CNN
F 3 "~" H 1900 2200 50  0001 C CNN
	1    1900 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT10
U 1 1 5DECE616
P 1900 2350
F 0 "NT10" V 1850 2200 50  0001 C CNN
F 1 "NT_BAT_LOGIC_NEG" V 1950 1950 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 1900 2350 50  0001 C CNN
F 3 "~" H 1900 2350 50  0001 C CNN
	1    1900 2350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3200 1900 3200 2200
Wire Wire Line
	3200 2350 3200 2700
Text Label 7950 2250 0    50   ~ 0
MCU_SECTION_POS
Text Label 7950 2350 0    50   ~ 0
MCU_SECTION_NEG
Text Label 2750 5100 0    50   ~ 0
MCU_SECTION_NEG
Text Label 2750 4700 0    50   ~ 0
MCU_SECTION_POS
Wire Notes Line
	2650 4350 6100 4350
Wire Notes Line
	2650 4250 6100 4250
Wire Notes Line
	2650 2900 6100 2900
Wire Notes Line
	2650 5200 6100 5200
Wire Wire Line
	2750 4700 3550 4700
Connection ~ 3550 4700
Wire Wire Line
	3550 5100 2750 5100
Connection ~ 3550 5100
Text Label 2750 3400 0    50   ~ 0
ANALOG_SECTION_POS
Text Label 2750 4100 0    50   ~ 0
ANALOG_SECTION_NEG
Wire Wire Line
	2750 4100 3650 4100
Wire Wire Line
	2750 3400 3750 3400
Connection ~ 3750 3400
$Comp
L Device:Net-Tie_2 NT16
U 1 1 5DF834BD
P 7900 2000
F 0 "NT16" V 7854 2044 50  0001 L CNN
F 1 "NT_LOGIC_MCUANALOG_POS" V 7945 2044 50  0001 L CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 7900 2000 50  0001 C CNN
F 3 "~" H 7900 2000 50  0001 C CNN
	1    7900 2000
	0    1    1    0   
$EndComp
$Comp
L Device:Net-Tie_2 NT17
U 1 1 5DF88D0E
P 7900 2600
F 0 "NT17" V 7800 2650 50  0001 L CNN
F 1 "NT_LOGIC_MCUANALOG_NEG" V 7900 2650 50  0001 L CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 7900 2600 50  0001 C CNN
F 3 "~" H 7900 2600 50  0001 C CNN
	1    7900 2600
	0    1    1    0   
$EndComp
$Comp
L Device:Net-Tie_2 NT19
U 1 1 5DFA5E4A
P 8250 2700
F 0 "NT19" V 8200 2800 50  0001 C CNN
F 1 "NT_LOGIC_GDRV_NEG" V 8300 3100 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 8250 2700 50  0001 C CNN
F 3 "~" H 8250 2700 50  0001 C CNN
	1    8250 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7900 2500 7900 2450
Wire Wire Line
	7900 2450 7950 2450
Wire Wire Line
	7950 2350 7900 2350
Wire Wire Line
	7900 2350 7900 2450
Connection ~ 7900 2450
Wire Wire Line
	7950 2250 7900 2250
Wire Wire Line
	7900 2250 7900 2150
Wire Wire Line
	7950 2150 7900 2150
Connection ~ 7900 2150
Wire Wire Line
	7900 2150 7900 2100
$Comp
L Device:Net-Tie_2 NT13
U 1 1 5DFC61C5
P 5650 4550
F 0 "NT13" H 5650 4750 50  0001 C CNN
F 1 "NT_MCUANALOG_MCU_POS" H 6000 4650 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5650 4550 50  0001 C CNN
F 3 "~" H 5650 4550 50  0001 C CNN
	1    5650 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT14
U 1 1 5DFC6993
P 5650 4950
F 0 "NT14" H 5600 5150 50  0001 C CNN
F 1 "NT_MCUANALOG_MCUA_NEG" H 6000 5050 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5650 4950 50  0001 C CNN
F 3 "~" H 5650 4950 50  0001 C CNN
	1    5650 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT15
U 1 1 5DFC6DC0
P 5650 5100
F 0 "NT15" V 5700 5000 50  0001 C CNN
F 1 "NT_MCUANALOG_MCU_NEG" V 5600 4600 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5650 5100 50  0001 C CNN
F 3 "~" H 5650 5100 50  0001 C CNN
	1    5650 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5100 5800 5100
Wire Wire Line
	5800 4950 5750 4950
Wire Wire Line
	5750 4550 5800 4550
$Comp
L Device:Net-Tie_2 NT11
U 1 1 5DFE845D
P 5650 3400
F 0 "NT11" H 5650 3600 50  0001 C CNN
F 1 "NT_MCUANALOG_ANALOG_POS" H 6050 3500 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5650 3400 50  0001 C CNN
F 3 "~" H 5650 3400 50  0001 C CNN
	1    5650 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT12
U 1 1 5DFE8AD0
P 5650 4100
F 0 "NT12" H 5650 4300 50  0001 C CNN
F 1 "NT_MCUANALOG_ANALOG_NEG" H 6050 4200 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5650 4100 50  0001 C CNN
F 3 "~" H 5650 4100 50  0001 C CNN
	1    5650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4100 5800 4100
Wire Wire Line
	5800 3400 5750 3400
Wire Wire Line
	2000 2350 2050 2350
Wire Wire Line
	2000 2200 2050 2200
Wire Wire Line
	4550 4550 5550 4550
$Comp
L Device:Net-Tie_2 NT18
U 1 1 5DFA6EE4
P 8250 1900
F 0 "NT18" V 8300 2000 50  0001 C CNN
F 1 "NT_LOGIC_GDRV_POS" V 8200 2300 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 8250 1900 50  0001 C CNN
F 3 "~" H 8250 1900 50  0001 C CNN
	1    8250 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1900 8400 1900
Connection ~ 7900 1900
Wire Wire Line
	8350 2700 8400 2700
Connection ~ 7900 2700
Wire Wire Line
	7500 2700 7900 2700
Wire Wire Line
	7500 1900 7900 1900
$Comp
L power:PWR_FLAG #FLG04
U 1 1 5E042106
P 7900 2150
F 0 "#FLG04" H 7900 2225 50  0001 C CNN
F 1 "PWR_FLAG" V 7900 2277 50  0001 L CNN
F 2 "" H 7900 2150 50  0001 C CNN
F 3 "~" H 7900 2150 50  0001 C CNN
	1    7900 2150
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG05
U 1 1 5E042E81
P 7900 2450
F 0 "#FLG05" H 7900 2525 50  0001 C CNN
F 1 "PWR_FLAG" H 7150 2650 50  0001 L CNN
F 2 "" H 7900 2450 50  0001 C CNN
F 3 "~" H 7900 2450 50  0001 C CNN
	1    7900 2450
	0    -1   -1   0   
$EndComp
Connection ~ 8400 2700
Wire Wire Line
	8400 2700 8500 2700
Connection ~ 8400 1900
Wire Wire Line
	8400 1900 8500 1900
Wire Wire Line
	7900 1900 8150 1900
Wire Wire Line
	7900 2700 8150 2700
Connection ~ 5750 4100
Connection ~ 5750 4950
Connection ~ 5750 5100
Wire Wire Line
	4800 5100 5550 5100
Wire Wire Line
	5150 4950 5550 4950
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5E05BBD3
P 5750 4550
F 0 "#FLG03" H 5750 4625 50  0001 C CNN
F 1 "PWR_FLAG" H 5750 4723 50  0001 C CNN
F 2 "" H 5750 4550 50  0001 C CNN
F 3 "~" H 5750 4550 50  0001 C CNN
	1    5750 4550
	-1   0    0    1   
$EndComp
Connection ~ 5750 4550
Wire Wire Line
	4950 4100 5550 4100
Connection ~ 5750 3400
Wire Wire Line
	5150 3400 5550 3400
Wire Wire Line
	1300 2200 1800 2200
Wire Wire Line
	1300 2350 1800 2350
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5E0626A4
P 2050 2200
F 0 "#FLG01" H 2050 2275 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 2373 50  0001 C CNN
F 2 "" H 2050 2200 50  0001 C CNN
F 3 "~" H 2050 2200 50  0001 C CNN
	1    2050 2200
	-1   0    0    -1  
$EndComp
Connection ~ 2050 2200
Wire Wire Line
	2050 2200 3200 2200
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5E062E29
P 2050 2350
F 0 "#FLG02" H 2050 2425 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 2523 50  0001 C CNN
F 2 "" H 2050 2350 50  0001 C CNN
F 3 "~" H 2050 2350 50  0001 C CNN
	1    2050 2350
	1    0    0    1   
$EndComp
Connection ~ 2050 2350
Wire Wire Line
	2050 2350 3200 2350
$EndSCHEMATC
